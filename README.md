# Pointing to a fork of a Go module

Example code to go alongside the blog post [Gotchas with pointing Go modules to a fork](https://www.jvt.me/posts/2023/09/20/go-mod-fork-gotcha/).

```sh
git clone https://gitlab.com/tanna.dev/jvt.me-examples/go-mod-fork-gotcha /tmp/go-mod-fork-gotcha

# will fail
go install gitlab.com/tanna.dev/jvt.me-examples/go-mod-fork-gotcha/with-replace@HEAD
# will pass
cd /tmp/go-mod-fork-gotcha/with-replace
go install

# will pass
go install gitlab.com/tanna.dev/jvt.me-examples/go-mod-fork-gotcha/without-replace@HEAD
# will pass
cd /tmp/go-mod-fork-gotcha/without-replace
go install
```
