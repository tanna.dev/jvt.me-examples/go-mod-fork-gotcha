module gitlab.com/tanna.dev/jvt.me-examples/go-mod-fork-gotcha/with-replace

go 1.21.1

require github.com/charmbracelet/log v0.2.4

require (
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/charmbracelet/lipgloss v0.7.1 // indirect
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.15.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
)

replace github.com/charmbracelet/log => github.com/jamietanna/log v0.2.2-0.20230912205513-bfd7186f150d
