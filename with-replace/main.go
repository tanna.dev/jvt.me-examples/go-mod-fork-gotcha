package main

import (
	"log/slog"
	"os"

	"github.com/charmbracelet/log"
)

func main() {
	handler := log.New(os.Stderr)
	logger := slog.New(handler)
	logger.Info("This is a Charm Bracelet logger, using slog")
}
